var x
var y

var hu = screen.height/100
var wu = screen.width/100

var player

/**@type {GameObject} */

var player1
var player2

var img

var PLAYER_IMG_RIGHT
var PLAYER_IMG_LEFT
var FOOTBALL_IMG
var BASKET_IMG
var BASKET_IMG2

var PLAYER_WIDTH
var PLAYER_HEIGHT

var player1ChosenImg = 1
var player2ChosenImg = 1

var MAX_SPEED_X
var MAX_SPEED_Y
var ACCL
var TERMINAL_VELOCITY

var canvas
var context
var moveimg

var runGame

var rightScore = 0
var leftScore = 0

var started = false
$(function () {
    $('.carousel').carousel('pause')
    $("#carouselPlayer1").on("slide.bs.carousel", function (e) {
        sessionStorage.setItem("player1ChosenImg", e.relatedTarget.id)
    })
    $("#carouselPlayer2").on("slide.bs.carousel", function (e) {
        sessionStorage.setItem("player2ChosenImg", e.relatedTarget.id)
    })
    $("#startBtn").on("click", start)
    if (sessionStorage.getItem("start")) {
        start()
        $("#gameScreen").show()
        $("#startScreen").hide()
    }
})
function start() {



    $(document).keydown(function (event) {
        var key = (event.keyCode ? event.keyCode : event.which)


        //  PLAYER 1 //


        // left
        if (key == '37') {
            player1.moveLeft = true
        }

        // right
        if (key == '39') {
            player1.moveRight = true
        }

        //up
        if (key == '38') {
            player1.jump()
        }


        //DASH

        // if (key == '17') {
        //     MAX_SPEED_X = 50
        //     setTimeout(function(){
        //         MAX_SPEED_X = 10
        //     }, 100)
        // }



        //  PLAYER 2 //


        // left
        if (key == '65') {
            player2.moveLeft = true
        }

        // right
        if (key == '68') {
            player2.moveRight = true
        }

        //up
        if (key == '87') {
            player2.jump()
        }


    })
    $(document).keyup(function (event) {
        var key = (event.keyCode ? event.keyCode : event.which)


        //PLAYER1 //


        // left
        if (key == '37') {
            player1.moveLeft = false
        }

        // right
        if (key == '39') {
            player1.moveRight = false
        }


        //PLAYER2 //


        // left
        if (key == '65') {
            player2.moveLeft = false
        }

        // right
        if (key == '68') {
            player2.moveRight = false
        }
    })



    init()
    // runGame = setInterval(run, 16.66667)
    window.requestAnimationFrame(run)
}

function init() {

    canvas = document.getElementById("canvas")
    canvas.setAttribute("width", screen.width)
    canvas.setAttribute("height", screen.height)
    context = canvas.getContext("2d")


    //FOOTBALL

    FOOTBALL_IMG = new Image()
    FOOTBALL_IMG.src = "media/basketball.png"

    football = new Football(FOOTBALL_IMG, 49*wu, 92.5*hu, 2*wu, 4*hu)

    //BASKET//

    BASKET_IMG = new Image()
    BASKET_IMG.src = "media/basketnew.png"

    BASKET_IMG2 = new Image()
    BASKET_IMG2.src = "media/basketnew2.png"

    basketLeft = new GameObject(BASKET_IMG2, -2.8*wu, 23*hu, 16*wu, 32*hu)
    basketRight = new GameObject(BASKET_IMG, 86.3*wu, 23*hu, 16*wu, 32*hu)

    //

    MAX_SPEED_X = 10
    MAX_SPEED_Y = -30
    TERMINAL_VELOCITY = 100

    ACCL = 1.5
    ACCL_X = 0.4

    if (sessionStorage.getItem("player1ChosenImg") != null)
        player1ChosenImg = sessionStorage.getItem("player1ChosenImg").slice(-1)
    if (sessionStorage.getItem("player2ChosenImg") != null)
        player2ChosenImg = sessionStorage.getItem("player2ChosenImg").slice(-1)


    //PLAYERS//

    PLAYER_IMG_RIGHT_1 = new Image()
    PLAYER_IMG_RIGHT_1.src = "media/duck.png"
    PLAYER_IMG_LEFT_1 = new Image()
    PLAYER_IMG_LEFT_1.src = "media/duck2.png"

    PLAYER_IMG_RIGHT_2 = new Image()
    PLAYER_IMG_RIGHT_2.src = "media/duckKotel.png"
    PLAYER_IMG_LEFT_2 = new Image()
    PLAYER_IMG_LEFT_2.src = "media/duckKotel2.png"

    PLAYER_IMG_RIGHT_3 = new Image()
    PLAYER_IMG_RIGHT_3.src = "media/duckHat.png"
    PLAYER_IMG_LEFT_3 = new Image()
    PLAYER_IMG_LEFT_3.src = "media/duckHat2.png"

    PLAYER_IMG_RIGHT_4 = new Image()
    PLAYER_IMG_RIGHT_4.src = "media/duckClass.png"
    PLAYER_IMG_LEFT_4 = new Image()
    PLAYER_IMG_LEFT_4.src = "media/duckClass2.png"

    PLAYER_IMG_RIGHT_5 = new Image()
    PLAYER_IMG_RIGHT_5.src = "media/duckThief.png"
    PLAYER_IMG_LEFT_5 = new Image()
    PLAYER_IMG_LEFT_5.src = "media/duckThief2.png"

    PLAYER_IMG_RIGHT_6 = new Image()
    PLAYER_IMG_RIGHT_6.src = "media/duckArab.png"
    PLAYER_IMG_LEFT_6 = new Image()
    PLAYER_IMG_LEFT_6.src = "media/duckArab2.png"

    PLAYER_IMG_RIGHT_7 = new Image()
    PLAYER_IMG_RIGHT_7.src = "media/duckThug.png"
    PLAYER_IMG_LEFT_7 = new Image()
    PLAYER_IMG_LEFT_7.src = "media/duckThug2.png"

    PLAYER_IMG_RIGHT_8 = new Image()
    PLAYER_IMG_RIGHT_8.src = "media/duckRamat.png"
    PLAYER_IMG_LEFT_8 = new Image()
    PLAYER_IMG_LEFT_8.src = "media/duckRamat2.png"

    PLAYER_IMG_RIGHT_9 = new Image()
    PLAYER_IMG_RIGHT_9.src = "media/duckEmo.png"
    PLAYER_IMG_LEFT_9 = new Image()
    PLAYER_IMG_LEFT_9.src = "media/duckEmo2.png"

    player1 = new Player(eval("PLAYER_IMG_LEFT_" + player1ChosenImg), 78*wu, 83*hu, 7*wu, 14*hu, "player1")
    player2 = new Player(eval("PLAYER_IMG_RIGHT_" + player2ChosenImg), 15*wu, 83*hu, 7*wu, 14*hu, "player2")
}

function run(milis) {
    update()
    render()
    colision()
    window.requestAnimationFrame(run)
}

function render() {

    context.clearRect(0, 0, canvas.width, canvas.height)
    player1.render(context)
    player2.render(context)
    football.render(context)
    basketLeft.render(context)
    basketRight.render(context)

    if (sessionStorage.getItem("start") === null)
        location.reload()
    sessionStorage.setItem("start", true);

}

function update() {
    player1.update()
    player2.update()
    football.update()
}

function colision() {

    //football //
    //========//

    player1.kickBall()
    player2.kickBall()

    football.scoreRight()
    football.scoreLeft()

    football.hitBoardRight()
    football.hitBoardLeft()

}

function GameObject(img, x, y, width, height, name) {
    this.img = img

    this.name = name

    this.x = x
    this.y = y
    this.width = width
    this.height = height
    this.moveRight = false
    this.moveLeft = false
    this.startedJumping = false
    this.speedY = 0
    this.speedX = 0
    this.faceRight
    this.punching
    this.movingX = false
    this.chosenImg

    this.maxX = function () {
        return this.x + this.width
    }

    this.maxY = function () {
        return this.y + this.height
    }

    this.calcSpeedY = function () {
        if (this.speedY < TERMINAL_VELOCITY)
            if (this === football)
                return football.speedY += ACCL * 0.8
        return this.speedY += ACCL * 1
    }
    this.willCollideRight = function () {
        if (this.x + this.width + this.speedX >= canvas.width) {
            //cant move right
            return true
        } else {
            return false
        }
    }

    this.willCollideLeft = function () {
        if (this.x - this.speedX <= 0) {
            //cant move left
            return true
        } else {
            return false
        }
    }


    this.willColllideTop = function () {
        if (this.y + this.speedY <= 0) {
            return true
        }
        return false
        
    }

}

//class Player
function Player(img, x, y, width, height, name) {
    GameObject.call(this, img, x, y, width, height, name)

}

GameObject.prototype.render = function (context) {
    if (this === basketLeft || this === player2) {
        context.filter = 'invert(1)'
    } else {
        context.filter = 'invert(0)'
    }
    context.drawImage(this.img, this.x, this.y, this.width, this.height)
}

GameObject.prototype.update = function () {

}

Player.prototype = Object.create(GameObject.prototype)
Player.prototype.constructor = Player

Player.prototype.render = function (context) {
    if (this === player2) {
        context.filter = 'invert(1)'
    } else {
        context.filter = 'invert(0)'
    }
    GameObject.prototype.render.call(this, context)
}

Player.prototype.willCollideBottom = function () {
    if (this.y + this.speedY >= 83*hu) {
        return true
    }
    return false
}

Player.prototype.jump = function () {
    // fly
    // if(!this.startedJumping){
    this.speedY = MAX_SPEED_Y
    this.startedJumping = true
    // }
}

Player.prototype.update = function () {
    GameObject.prototype.update.call(this, null)
    if (this.moveRight) {
        this.faceRight = true
        this.img = eval("PLAYER_IMG_RIGHT_" + eval(this.name + "ChosenImg"))

        if (this.willCollideRight()) {
            this.x = canvas.width - this.width
        } else {
            this.x += MAX_SPEED_X
        }
    }
    if (this.moveLeft) {
        this.faceRight = false
        this.img = eval("PLAYER_IMG_LEFT_" + eval(this.name + "ChosenImg"))

        if (this.willCollideLeft()) {
            this.x = 0
        } else {
            this.x -= MAX_SPEED_X
        }
    }

    if (this.startedJumping) {
        if (this.willColllideTop()) {
            this.y = 0
            this.speedY = 0
        }

        if (!this.willCollideBottom()) {
            this.calcSpeedY()
            this.y += this.speedY
        } else {
            this.y = 83*hu
            this.speedY = 0
            this.startedJumping = false
        }
    }


}

Player.prototype.kickBall = function () {

    if (football.x > this.x - 35 && football.maxX() < this.maxX() + 35 && football.y > this.y && football.maxY() < this.maxY()) {
        football.getSpeed(this.faceRight, this.speedY)
    }
}


// class Football//


function Football(img, x, y, width, height) {
    GameObject.call(this, img, x, y, width, height)

}

Football.prototype = Object.create(GameObject.prototype)
Football.prototype.constructor = Football

Football.prototype.willCollideBottom = function () {
    if (this.y + this.speedY >= 92.5*hu) {
        return true
    }
    return false
}

Football.prototype.getSpeed = function (kickRight, kickY) {
    if (!kickRight) {
        this.speedX = -MAX_SPEED_X * 1.5
        this.movingX = true
    } else {
        this.speedX = MAX_SPEED_X * 1.5
        this.movingX = true
    }
    this.speedY = kickY - 20
    this.startedJumping = true
}

Football.prototype.calcSpeedX = function () {
    if (this.speedX > 0) {
        this.speedX -= ACCL_X * 1
        if (this.speedX <= 0)
            this.movingX = false
    }
    if (this.speedX < 0) {
        this.speedX += ACCL_X * 1
        if (this.speedX >= 0)
            this.movingX = false
    }
}

Football.prototype.willCollideBottom = function () {
    if (this.y + this.speedY >= 93*hu) {
        return true
    }
    return false
}


Football.prototype.update = function () {
    GameObject.prototype.update.call(this, null)
    if (this.movingX) {
        this.calcSpeedX()
        this.x += this.speedX
        if (this.willCollideRight()) {
            this.x = canvas.width - this.width
            this.speedX = -20
        } else {
            this.x += MAX_SPEED_X
        }
        if (this.willCollideLeft()) {
            this.x = 0
            this.speedX = 20
        } else {
            this.x -= MAX_SPEED_X
        }



    }
    if (this.startedJumping) {
        if (this.willColllideTop()) {
            this.y = 0
            this.speedY = 0
        }

        if (!this.willCollideBottom()) {
            ACCL_X = 0.15
            this.calcSpeedY()
            this.y += this.speedY
        } else {
            ACCL_X = 0.15
            this.y = 92.5*hu
            this.speedY = -this.speedY
            // this.startedJumping = false
        }
    }
}


Football.prototype.scoreRight = function () {
    if (football.speedY > 0 && football.y > basketRight.y + 170 && football.y < basketRight.y + 200 && football.x > basketRight.x + 10 && football.maxX() < basketRight.maxX() - 80) {
        leftScore++
        $("#score2").text(leftScore)
        $("#explo1").css("display", "block")
        football.hide()
        setTimeout(() => {
            $("#explo1").css("display", "none")
            if (leftScore === 10)
                endScreen(player2)
            else
                init()
        }, 1400)
    }
}

Football.prototype.scoreLeft = function () {
    if (football.speedY > 0 && football.y > basketLeft.y + 170 && football.y < basketLeft.y + 200 && football.x > basketLeft.x + 80 && football.maxX() < basketLeft.maxX()) {
        rightScore++
        $("#score1").text(rightScore)
        $("#explo2").css("display", "block")
        football.hide()
        setTimeout(() => {
            $("#explo2").css("display", "none")
            if (rightScore === 10)
                endScreen(player1)
            else
                init()
        }, 1400)
    }
}

Football.prototype.hitBoardRight = function () {
    if (football.y < basketRight.y + 200 && football.maxX() > basketRight.maxX() - 80) {
        football.speedX = -football.speedX
    }
}

Football.prototype.hitBoardLeft = function () {
    if (football.y < basketLeft.y + 200 && football.x < basketLeft.x + 80) {
        football.speedX = -football.speedX
    }
}

Football.prototype.hide = function () {
    football.height = 0
    football.speedY = 0
    football.y = 92.5*hu
}


function endScreen(winner) {
    context.clearRect(0, 0, canvas.width, canvas.height)
    $(".score").css("display", "none")
    clearInterval(runGame)
    $("#endLine").show().text(winner.name + " won!")
    $("#restartBtn").show().on("click", function () { location.reload() })
}
